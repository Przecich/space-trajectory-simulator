package feonr;

import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Mesh;
import com.jme3.scene.VertexBuffer;
import com.jme3.util.BufferUtils;

import java.nio.FloatBuffer;

public class Circle extends Mesh {

    private  float minorAxis = 0;
    private Vector3f center;


    private float radius;

    private int samples;

    private float angle;

    public Circle(float radius) {
        this(Vector3f.ZERO, radius, 16,2*FastMath.PI);
    }

    public Circle(float radius, int samples,float angle) {
        this(Vector3f.ZERO, radius, samples, angle);
    }

    public Circle(Vector3f center, float radius, int samples, float angle) {

        super();

        this.center = center;

        this.radius = radius;

        this.samples = samples;

        this.angle = angle;


        setMode(Mode.LineStrip);

        updateGeometry();
    }

    public Circle(Vector3f center, float radius, float minorAxis, int samples, float angle) {

        super();

        this.center = center;

        this.radius = radius;

        this.minorAxis = minorAxis;

        this.samples = samples;

        this.angle = angle;


        setMode(Mode.LineStrip);

        updateGeometry();
    }


    protected void updateGeometry() {
        FloatBuffer positions = BufferUtils.createFloatBuffer(samples * 3);
        FloatBuffer normals = BufferUtils.createFloatBuffer(samples * 3);

        short[] indices = new short[samples +1];
        System.out.println("GEOM");

        float rate = FastMath.TWO_PI / (float) samples;

        float angle = 7.85098163397f;
        for (int i = 0; i < samples; i++) {
            float minor = this.minorAxis != 0 ? this.minorAxis : radius;
            float x = (float)(minor/(Math.sqrt((minor*minor*(Math.tan(angle)*Math.tan(angle))/(radius*radius)+1))));

            float z = (float)(minor*Math.tan(angle)/(Math.sqrt((minor*minor*(Math.tan(angle)*Math.tan(angle)))/(radius*radius)+1)));
            if(angle<4.72){
                x=-x;
                z=-z;
            }
            System.out.println("ID: "+ i+ " x: "+x+" y: "+z+" ANGLE: "+angle);
            positions.put(x +center.x).put(center.y).put(z+center.z);
            normals.put(new float[]{0, 1, 0});

            indices[i] = (short) i;
            if((this.angle!=0 && angle<7.7911525f-this.angle) || angle<1.60){
                System.out.println("ANGLE: "+angle);
                System.out.println(this);
                System.out.println("BREAK: "+(angle)*(180/FastMath.PI)+" z:"+z+ " z full: "+z * radius+center.z+" x full "+x * minor+center.x);
                System.out.println("TRUE ANGLE"+FastMath.atan(minor/radius*FastMath.tan(angle)));
                break;
            }
            angle-=rate;
        }

        setBuffer(VertexBuffer.Type.Position, 3, positions);

        setBuffer(VertexBuffer.Type.Normal, 3, normals);

        setBuffer(VertexBuffer.Type.Index, 2, indices);

        updateBound();
    }
}