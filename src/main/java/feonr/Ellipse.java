package feonr;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import de.lessvoid.nifty.tools.Color;
import feonr.logic.model.physicobject.model.Orbit;


//THOUGHT: CREATE NORMAL ORBIT AND PATH At the same time, try simmilar rotation
public class Ellipse {
    public static int id = 0;
    public static Geometry of(Vector3f center, Orbit orbit, AssetManager assetManager, float angle) {
        Geometry geometry = new Geometry("Orbit: "+id, createCircularOrbit(center, orbit,angle));
        rotateToAscendingNode(geometry, orbit);
        changeInclination(geometry, orbit);
        rotatePeriapsis(geometry, orbit);
        setFocusPoint(geometry, orbit);
        geometry.setMaterial(getMaterial(assetManager, ColorRGBA.Orange));
        id++;
        return geometry;
    }

    public static Geometry of(Vector3f center, Orbit orbit, AssetManager assetManager, float angle, ColorRGBA color, String name) {
        Geometry geometry = new Geometry(name, createCircularOrbit(center, orbit,angle));
        rotateToAscendingNode(geometry, orbit);
        changeInclination(geometry, orbit);
        rotatePeriapsis(geometry, orbit);
        setFocusPoint(geometry, orbit);
        geometry.setMaterial(getMaterial(assetManager,color));
        id++;

        return geometry;
    }

    private static void rotateToAscendingNode(Geometry geometry, Orbit orbit) {
        geometry.rotate(0, (float) orbit.getLongitude(), 0);
    }

    private static void changeInclination(Geometry geometry, Orbit orbit) {
        geometry.rotate(0, 0, -(float) orbit.getInclination());
    }

    private static void rotatePeriapsis(Geometry geometry, Orbit orbit) {
        geometry.rotate(0, (float) orbit.getPeriapsis(), 0);
    }

    private static void setFocusPoint(Geometry geometry, Orbit orbit) {
        float focus = (float) (orbit.getEccentricity() * orbit.getSemiMajorAxis());
        geometry.setLocalTranslation(geometry.getLocalRotation().mult(new Vector3f(0, 0, -focus)));
    }

    private static Circle createCircularOrbit(Vector3f center, Orbit orbit) {
        return new Circle(center, (float) orbit.getSemiMajorAxis(), 100,0);
    }

    private static Circle createCircularOrbit(Vector3f center, Orbit orbit, float angle) {
        return new Circle(center, (float) orbit.getSemiMajorAxis(),(float)orbit.getMinorAxis(), 100,angle);
    }

    private static Material getMaterial(AssetManager assetManager, ColorRGBA color) {
        Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        material.setColor("Color", color);
        return material;
    }
}
