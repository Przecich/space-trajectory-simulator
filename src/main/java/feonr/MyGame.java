package feonr;

import com.jme3.app.SimpleApplication;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.system.AppSettings;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyEventSubscriber;
import de.lessvoid.nifty.builder.LayerBuilder;
import de.lessvoid.nifty.builder.PanelBuilder;
import de.lessvoid.nifty.builder.PopupBuilder;
import de.lessvoid.nifty.builder.ScreenBuilder;
import de.lessvoid.nifty.controls.ListBox;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.controls.TextFieldChangedEvent;
import de.lessvoid.nifty.controls.button.builder.ButtonBuilder;
import de.lessvoid.nifty.controls.label.builder.LabelBuilder;
import de.lessvoid.nifty.controls.listbox.builder.ListBoxBuilder;
import de.lessvoid.nifty.controls.textfield.builder.TextFieldBuilder;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import de.lessvoid.nifty.tools.Color;
import feonr.logic.model.physicobject.model.Orbit;
import feonr.states.universe.UniverseState;

public class MyGame extends SimpleApplication implements ScreenController {

    Nifty nifty;
    UniverseState universeState;
    Element popupAddOrbit;
    Element popupEdditOrbir;
    Element popupManeuver;

    public static void main(String[] args) {
        MyGame app = new MyGame();

        initializeSettings(app);

        app.start(); // start the game
    }

    @Override
    public void simpleInitApp() {

        NiftyJmeDisplay niftyDisplay = NiftyJmeDisplay.newNiftyJmeDisplay(
                assetManager,
                inputManager,
                audioRenderer,
                guiViewPort);

        nifty = niftyDisplay.getNifty();
        guiViewPort.addProcessor(niftyDisplay);
        flyCam.setDragToRotate(true);

        nifty.loadStyleFile("nifty-default-styles.xml");
        nifty.loadControlFile("nifty-default-controls.xml");

        // <screen>
        nifty.addScreen("Screen_ID", new ScreenBuilder("Hello Nifty Screen") {{
            controller(MyGame.this); // Screen properties

            // <layer>
            layer(new LayerBuilder("Layer_ID") {{
                childLayoutVertical(); // layer properties, add more...

                // <panel>
                panel(new PanelBuilder("Side-Menu-Panel") {{
                    alignRight();
                    valignBottom();
                    childLayoutVertical();


                    // GUI elements
                    control(new ButtonBuilder("Button", "Add orbit") {{
                        alignRight();
                        valignBottom();
                        height("5%");
                        interactOnClick("addOrbitPopup()");
                        width("5%");
                    }});

                    control(new ButtonBuilder("Button2", "Edit orbit") {{
                        alignRight();
                        valignBottom();
                        height("5%");
                        interactOnClick("editOrbitPopup()");
                        width("5%");
                    }});

                }});
                // </panel>
            }});
            // </layer>
        }}.build(nifty));
        // </screen>

        createAddOrbitPopup("70", "0", "0", "0", "0", "popupExit");


        new PopupBuilder("popupManevour") {{
            childLayoutCenter();
            // <panel>
            panel(new PanelBuilder("Panel_ID-3") {{
                style("nifty-panel");
                alignCenter();
                width("15%");
                height("15%");
                childLayoutVertical();
                control(new LabelBuilder("edit-orbit", "Delta v(m/s") {{
                    width("50%");
                    alignCenter();
                }});
                control(new TextFieldBuilder("manevour-textfield", "1000") {{ // init with the text "*"
                    maxLength(6); // force only a single character input
                    width("50%");
                    alignCenter();
                    backgroundColor(Color.BLACK);
                }});
                control(new ButtonBuilder("add-maneuver-button", "Add") {{
                    interactOnClick("addManeuver()");
                    width("33%");
                    alignCenter();
                }});


            }});
            // </panel>
            backgroundColor("#000a");
        }}.registerPopup(nifty);

        new PopupBuilder("popupEdit") {{
            childLayoutCenter();
            // <panel>
            panel(new PanelBuilder("Panel_ID-2") {{
                style("nifty-panel");
                alignCenter();
                width("20%");
                height("35%");
                childLayoutVertical();
                control(new LabelBuilder("edit-orbit", "Edit orbit") {{
                    width("50%");
                    alignCenter();
                }});
                control(new ListBoxBuilder("orbit-list") {{

                    selectionModeSingle();
                    displayItems(5);
                    optionalHorizontalScrollbar();
                    optionalVerticalScrollbar();
                    width("*"); // standard nifty width attribute
                }});
                panel(new PanelBuilder("horizontal-buttons") {{
                    childLayoutHorizontal();
                    alignCenter();
                    control(new ButtonBuilder("edit-button", "Edit orbit") {{
                        interactOnClick("editOrbit()");
                        width("32%");
                        alignLeft();
                    }});
                    control(new ButtonBuilder("delete-button", "Delete orbit") {{
                        interactOnClick("deleteOrbit()");
                        width("33%");
                        alignCenter();
                    }});

                    control(new ButtonBuilder("cancel-button", "Cancel") {{
                        interactOnClick("cancelOrbit()");
                        width("32%");
                        alignRight();
                    }});
                }});
                control(new ButtonBuilder("maneuver-button", "Add maneuver") {{
                    interactOnClick("addManeuverPopup()");
                    width("50%");
                    alignCenter();
                }});


            }});
            // </panel>
            backgroundColor("#000a");
        }}.registerPopup(nifty);


        nifty.gotoScreen("Screen_ID"); // start the screen
        universeState = new UniverseState(this);
        stateManager.attach(universeState);
    }

    private static void initializeSettings(MyGame app) {
        app.setShowSettings(false);
        AppSettings settings = new AppSettings(true);
        settings.setFrameRate(60);
        settings.put("Width", 1280);
        settings.put("Height", 720);
        settings.put("Title", "My awesome Game");
        settings.put("VSync", true);

        settings.put("Samples", 0);

        app.setSettings(settings);
    }

    public void addOrbitPopup() {
        popupAddOrbit = nifty.createPopup("popupExit");
        nifty.showPopup(nifty.getCurrentScreen(), popupAddOrbit.getId(), null);
    }

    public void editOrbitPopup() {
        popupEdditOrbir = nifty.createPopup("popupEdit");
        ListBox listBox = popupEdditOrbir.findNiftyControl("orbit-list", ListBox.class);
        universeState.universe.getOrbitsIds().keySet().stream().map(x -> getRootNode().getChild(x).getName())
                .forEach(listBox::addItem);

        nifty.showPopup(nifty.getCurrentScreen(), popupEdditOrbir.getId(), null);
        System.out.println(nifty.getCurrentScreen());

        System.out.println(listBox);
    }

    public void editOrbit() {
        delete();
        nifty.showPopup(nifty.getCurrentScreen(), popupAddOrbit.getId(), null);
    }

    public void addOrbit() {
        Orbit orbit = new Orbit.OrbialBuilder()
                .withEccentricity(getTextFieldValue("eccentricity-textfield"))
                .withInclination(getTextFieldValue("inclination-textfield"))
                .withLongitude(getTextFieldValue("longitude-textfield"))
                .withMajorAxis(getTextFieldValue("major-axis-textfield"))
                .withPeriapsis(getTextFieldValue("periapsis-textfield"))
                .build();

        universeState.universe.addObject(orbit);
        nifty.closePopup(popupAddOrbit.getId());
        System.out.println("elo");
        System.out.println(orbit.calculatePosition(0));
    }

    public void addManeuverPopup() {
        popupManeuver = nifty.createPopup("popupManevour");
        nifty.showPopup(nifty.getCurrentScreen(), popupManeuver.getId(), null);
    }

    public void addManeuver() {
        ListBox list = nifty.getCurrentScreen().findNiftyControl("orbit-list", ListBox.class);
        String selection = (String) list.getSelection().get(0);


        universeState.universe.addPath(selection, getTextFieldValue("manevour-textfield"));
        nifty.closePopup(popupManeuver.getId());
    }

    public void deleteOrbit() {
        delete();
    }

    public void cancelOrbit() {
        nifty.closePopup(popupEdditOrbir.getId());
    }

    public void bind(Nifty nifty, Screen screen) {
        System.out.println("bind( " + screen.getScreenId() + ")");
    }


    public void onStartScreen() {
        System.out.println("onStartScreen");
    }


    public void onEndScreen() {
        System.out.println("onEndScreen");
    }

    public void quit() {
        nifty.gotoScreen("end");
    }

    @NiftyEventSubscriber(id = "major-axis-textfield")
    public void onTextChange(final String id, final TextFieldChangedEvent event) {
        System.out.println(event.getText());
    }

    private double getTextFieldValue(String id) {
        return Double.parseDouble(nifty.getCurrentScreen().findNiftyControl(id, TextField.class).getText());
    }

    private PanelBuilder createVerticalPanel(String id, String name, String initialValue) {
        return new PanelBuilder(id + "-panel") {{
            childLayoutHorizontal();
            control(new LabelBuilder(id, name) {{
                width("50%");
                alignRight();
            }});
            control(new TextFieldBuilder(id + "-textfield", initialValue) {{ // init with the text "*"
                maxLength(6); // force only a single character input
                width("50%");
                alignCenter();
                backgroundColor(Color.BLACK);
            }});

        }};
    }

    private void createAddOrbitPopup(String major_axis, String eccentricity, String inclination, String periapsis, String longitude, String name) {
        new PopupBuilder(name) {{
            childLayoutCenter();
            // <panel>
            panel(new PanelBuilder("Panel_ID" + name) {{
                style("nifty-panel");
                alignCenter();
                width("20%");
                height("27%");
                childLayoutVertical();
                panel(createVerticalPanel("major-axis", "Semi major axis", major_axis));
                panel(createVerticalPanel("eccentricity", "Eccentricity", eccentricity));
                panel(createVerticalPanel("inclination", "Inclination", inclination));
                panel(createVerticalPanel("periapsis", "Periapsis", periapsis));
                panel(createVerticalPanel("longitude", "Longitude", longitude));
                control(new ButtonBuilder("add-button", "Add orbit") {{
                    interactOnClick("addOrbit()");
                    alignCenter();
                }});
            }});
            // </panel>
            backgroundColor("#000a");
        }}.registerPopup(nifty);
    }

    private void delete() {
        ListBox list = nifty.getCurrentScreen().findNiftyControl("orbit-list", ListBox.class);
        String selection = (String) list.getSelection().get(0);
        universeState.getUniverseNode().detachChild(rootNode.getChild(selection));

        universeState.universe.getOrbitsIds().remove(selection);
        list.removeItem(selection);
        nifty.closePopup(popupEdditOrbir.getId());
    }
}