package feonr;

import java.util.Optional;
import java.util.function.Function;

public class Solver {
    public static double derrivative(Function<Double,Double> function, double point){
        double e=3600;
        return (function.apply(point+e)-function.apply(point))/e;
    }

    public static Optional<Double> newtonRootSolver(Function<Double,Double> function, double point){
        double e =0.01;
        double result = point - function.apply(point)/derrivative(function,point);
        int iterration = 0;
        while(Math.abs(function.apply(result))>e && iterration<1000){
            result = result - function.apply(result)/derrivative(function,result);
            iterration++;
        }
        return iterration >= 1000 ? Optional.empty() : Optional.of(result);
    }

    public static double keplerEquation(double e, double M){
        double E = M;
        for (int i = 0; i < 100; i++) {
            E = M + e*Math.sin(E);
        }
        return E;
    }
}
