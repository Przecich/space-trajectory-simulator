package feonr.logic.model;

public class Constants {
    public static double Gravity = 6.674e-11;
    public static final double GM = Gravity*1.989e30;
}
