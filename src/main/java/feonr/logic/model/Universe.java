package feonr.logic.model;

import com.jme3.asset.AssetManager;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import feonr.logic.model.physicobject.Path;
import feonr.logic.model.physicobject.Planet;
import feonr.logic.model.physicobject.SpaceObject;
import feonr.logic.model.physicobject.model.Orbit;

import java.util.*;

public class Universe {
    private HashMap<String, SpaceObject> objects;
    private HashMap<String, Path> orbitsIds;
    private static final int timeMultimplier = 3600 * 24 * 5;
    private UniverseFactory universeFactory;
    private Node universeNode;
    double stat = 0;
    private boolean simulationRun = true;


    public Universe(Node universeNode, AssetManager assetManager) {
        universeFactory = new UniverseFactory(assetManager, universeNode);
        objects = universeFactory.createSolarSystem();
        orbitsIds = universeFactory.getOrbits();
        this.universeNode = universeNode;
    }

    public HashMap<String, SpaceObject> getObjects() {
        return objects;
    }

    public void update(float tpf) {
        if(simulationRun) {
            engineRun(tpf);
        }
    }

    public void addPath(String selection, double velocityAdd) {
        Path path = getOrbitsIds().get(selection);

        Orbit orbit = path.getOrbit();
        double radius = orbit.calculateRadius(0) * 1e9;
        double velocity = Math.sqrt(Constants.GM * (2 / (radius) - 1 / (orbit.getSemiMajorAxis() * 1e9)));
        velocity += velocityAdd;
        double majorAxis = 1 / (-((velocity * velocity) / Constants.GM) + 2 / (radius));
        double e = (2 * majorAxis - 2 * radius) / (2 * majorAxis);
        System.out.println(Constants.GM);
        System.out.println(orbit.calculateRadius(0));
        System.out.println(Constants.GM * (2 / (orbit.calculateRadius(0) * 1e9) - 1 / (orbit.getSemiMajorAxis() * 1e9)));
        System.out.println("V: " + velocity);
        System.out.println("a: " + majorAxis);
        System.out.println("e: " + e);

        Orbit orbit2 = new Orbit.OrbialBuilder()
                .withEccentricity(e)
                .withInclination(orbit.getInclination())

                .withMajorAxis(majorAxis / 1e9)
                .withPeriapsis(orbit.getPeriapsis())
                .withLongitude(orbit.getLongitude())
                .build();

        Optional<Double> encounterTime; //= calcEncounterTime(orbit2, "Venus");
/*        Optional<Double> encounterTime2 = calcEncounterTime(orbit2, "Earth");
        Optional<Double> encounterTime3 = calcEncounterTime(orbit2, "Mars");*/

        List<Optional<Double>> results = Arrays.asList(
                calcEncounterTime(orbit2, "Venus"),
                calcEncounterTime(orbit2, "Earth"),
                calcEncounterTime(orbit2, "Mars"));

        encounterTime = results.stream().filter(Optional::isPresent).map(Optional::get).findFirst();

        if (encounterTime.isPresent()) {
            double trueAnomally = orbit2.calculateTrueAnomally(Constants.GM, encounterTime.get());
            path.setPredictedPath(orbit2, (float) orbit2.calculateCenterAngle(trueAnomally));
        } else {
            path.setPredictedPath(orbit2, 2 * FastMath.PI);
        }
    }

    public void addObject(Orbit orbit) {
        universeFactory.addObject(orbit);
        /*engineRun2(0.016f,orbit);*/
    }

    public HashMap<String, Path> getOrbitsIds() {
        return orbitsIds;
    }


    private void engineRun(float tpf) {
        //add here the object and everythign will be fine
        List<SpaceObject> obj = new ArrayList<>(objects.values());
        for (int i = 0; i < obj.size(); i++) {
            for (int j = 0; j < obj.size(); j++) {
                if (i != j) {
                    obj.get(i).addForce(caluclateForce(obj.get(j), obj.get(i)));
                }
            }
        }
        obj.forEach(x -> x.move(tpf * timeMultimplier));
        stat+=tpf * timeMultimplier;
        System.out.println(stat);
        obj.forEach(x -> x.rotate(tpf));
    }


    private void engineRun2(float tpf, Orbit orbit) {
        //add here the object and everythign will be fine
        List<SpaceObject> obj = new ArrayList<>(objects.values());
        double time = 6666766;
        double radius = orbit.calculateRadius(0) * 1e9;
        double vel = Math.sqrt(Constants.GM * (2 / (radius) - 1 / (orbit.getSemiMajorAxis() * 1e9)));

        Vector3f a = orbit.calculatePosition(0);
        long startTimeFirst = System.nanoTime();
        Vector3f finall = orbit.calculatePosition((float)orbit.calculateTrueAnomally(Constants.GM,time));
        long durationFirst = System.nanoTime() - startTimeFirst;

        Vector3f b = a.mult(1e9f);
        Vector3f vekicity = orbit.calculateVelocity(0);
        SpaceObject obj2 = new Planet(new SpaceObject.SpaceObjectBuilder("Earth")
                .withTexturePath("Textures/2k_earth_daymap.jpg")
                .withNormalTexturePath("Textures/2k_earth_normal_map.jpg")
                .withPosition(new Vector3f(b.y,b.z,b.x))
                .withApparentRadius(4)
                .withMass(2000)
                .withRotationSpeed(0.5f)
                .withRadius(10)
                .withVelocity(new Vector3f(-vekicity.y,vekicity.z,-vekicity.x)));
        obj.add(obj2);

        System.out.println("ENGINE RUN @");
        long startTimeSecond = System.nanoTime();
        int timeMultimplier = 9;

        while(time>0) {
            for (int i = 0; i < obj.size(); i++) {
                for (int j = 0; j < obj.size(); j++) {
                    if (i != j) {
                        obj.get(i).addForce(caluclateForce(obj.get(j), obj.get(i)));
                    }
                }
            }
            obj.forEach(x -> x.move2(tpf*timeMultimplier));
            stat+=tpf * timeMultimplier;
            time -= tpf * (timeMultimplier);
        }
        long durationSecond = System.nanoTime() - startTimeSecond;
        System.out.println(durationFirst);
        System.out.println(durationSecond);
        //System.out.println(obj2.getPosition());
        Vector3f nbodu = new Vector3f(obj2.getPosition().z/1e9f,obj2.getPosition().x/1e9f,obj2.getPosition().y);
        System.out.println("N BODY:    "+nbodu.x+","+nbodu.y+","+nbodu.z+","+durationSecond);
        System.out.println("MY:    "+finall.x+","+finall.y+","+finall.z+","+durationFirst);
        System.out.println("ALL:    "+nbodu.x+","+nbodu.y+","+nbodu.z+","+durationSecond+", ,"+finall.x+","+finall.y+","+finall.z+","+durationFirst);
        Vector3f error = nbodu.subtract(finall);
    }

    private Vector3f caluclateForce(SpaceObject obj, SpaceObject obj2) {
        final double G = 6.67408e-11;
        double distance = obj.getPosition().distance(obj2.getPosition());
        float force = (float) ((G * obj.getMass() * obj2.getMass()) / (distance * distance));
        return (obj.getPosition().subtract(obj2.getPosition())).normalize().mult(force);
    }

    private Optional<Double> calcEncounterTime(Orbit orbit2, String name) {
        System.out.println("________");
        System.out.println(name);
        System.out.println("-------");
        Orbit orbit3 = ((Planet) getObjects().get(name)).getPath();
        Optional<Double> encounterTime = orbit2.calculateMinDistance2(Constants.GM, orbit3);
        System.out.println(encounterTime.orElse(0.0));
        System.out.println(orbit2.caluclateDistance(Constants.GM, orbit3, encounterTime.orElse(0.0).floatValue()));
        return orbit2.calculateMinDistance2(Constants.GM, orbit3);
    }

    public boolean isSimulationRun() {
        return simulationRun;
    }

    public void setSimulationRun(boolean simulationRun) {
        this.simulationRun = simulationRun;
    }
}
