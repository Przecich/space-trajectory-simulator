package feonr.logic.model;

import com.jme3.asset.AssetManager;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import feonr.Ellipse;
import feonr.logic.model.physicobject.Path;
import feonr.logic.model.physicobject.Planet;
import feonr.logic.model.physicobject.SpaceObject;
import feonr.logic.model.physicobject.Star;
import feonr.logic.model.physicobject.model.Orbit;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class UniverseFactory {
    private final AssetManager assetManager;
    private final Node universeNode;
    private HashMap<String, SpaceObject> objects = new HashMap<>();
    private HashMap<String,Path> orbitsIds = new HashMap<>();

    UniverseFactory(AssetManager assetManager, Node universeNode) {
        this.assetManager = assetManager;
        this.universeNode = universeNode;
    }

    HashMap<String, SpaceObject> createSolarSystem() {
        createSun();
        SpaceObject earth = createEarth();
        universeNode.attachChild(earth.getOrbit());

        SpaceObject mars = createMars();
        universeNode.attachChild(mars.getOrbit());

        SpaceObject venus = createVenus();
        universeNode.attachChild(venus.getOrbit());

        SpaceObject mercury = createMercury();
        universeNode.attachChild(mercury.getOrbit());

        SpaceObject jupiter = createJupiter();
        universeNode.attachChild(jupiter.getOrbit());

        return objects;
    }

    public HashMap<String,Path> getOrbits() {
        return orbitsIds;
    }

    public void addObject(Orbit oribit) {
        double time = oribit.calculateEscapeTime(Constants.Gravity * (objects.get("Sun").getMass()));
        double trueAnomally = oribit.calculateTrueAnomally(Constants.Gravity * (objects.get("Sun").getMass()), time);
        Path path = new Path( oribit, assetManager, universeNode);
        orbitsIds.put(path.getName(),path);

  /*      if (trueAnomally > 0)
            path.setPredictedPath(oribit,  (float) oribit.calculateCenterAngle(trueAnomally));*/
        path.addExistingPaths(universeNode);
    }
/*

    private void addTestOrbits() {
        Geometry geometry = Ellipse.of(Vector3f.ZERO, new Orbit.OrbialBuilder()
                .withMajorAxis(60)
                .withEccentricity(0)
                .withLongitude(0)
                .withInclination(FastMath.PI)
                .withPeriapsis(0)
                .build(), assetManager);


        universeNode.attachChild(geometry);
        Orbit orbit = new Orbit.OrbialBuilder().withMajorAxis(87.909050).withEccentricity(0.5056).withPeriapsis(0.43).withLongitude(0.1).withInclination(FastMath.PI / 4).build();
        double time = orbit.calculateEscapeTime(Constants.Gravity * (objects.get("Sun").getMass()));
        System.out.println("Time: " + (time / (60 * 60 * 24)));
        System.out.println("True anomally: " + orbit.calculateTrueAnomally(Constants.Gravity * (objects.get("Sun").getMass()), time));
        universeNode.attachChild(Ellipse.of(Vector3f.ZERO, orbit, assetManager));
    }
*/

    private void createSun() {
        addObject("Sun", new Star(new SpaceObject.SpaceObjectBuilder("Sun")
                .withTexturePath("Textures/2k_sun.jpg")
                .withPosition(Vector3f.ZERO)
                .withApparentRadius(10)
                .withAssetManager(assetManager)
                .withMass(1.989e30)
                .withRadius(10)
                .withVelocity(new Vector3f(0, 0, 0))));
    }

    private SpaceObject createMercury() {
        Vector3f mercuryStart = new Vector3f(0, 0, 57_910_000_000f);

        Planet mercury = new Planet(new SpaceObject.SpaceObjectBuilder("Mercury")
                .withTexturePath("Textures/mercury_diffuse.jpg")
                .withNormalTexturePath("Textures/mercury_normal.jpg")
                .withPosition(mercuryStart)
                .withApparentRadius(2)
                .withAssetManager(assetManager)
                .withMass(3.285E23)
                .withRotationSpeed(0.5f)
                .withRadius(9)
                .withVelocity(new Vector3f(47362, 0, 0)));
        addObject("Mercury", mercury);
        return mercury;
    }

    private SpaceObject createVenus() {
        Vector3f earthStart = new Vector3f(0, 0, 108_210_000_000f);

        Planet venus = new Planet(new SpaceObject.SpaceObjectBuilder("Venus")
                .withTexturePath("Textures/venus_diffuse.jpg")
                .withNormalTexturePath("Textures/venus_normal.jpg")
                .withPosition(earthStart)
                .withApparentRadius(4)
                .withAssetManager(assetManager)
                .withMass(5.972E24)
                .withRotationSpeed(0.5f)
                .withRadius(9)
                .withVelocity(new Vector3f(35020, 0, 0)));
        addObject("Venus", venus);
        return venus;
    }

    private SpaceObject createEarth() {
        Vector3f earthStart = new Vector3f(0, 0, 149_600_000_000f);
        float a = earthStart.length()/1e9f;

        Planet earth = new Planet(new SpaceObject.SpaceObjectBuilder("Earth")
                .withTexturePath("Textures/2k_earth_daymap.jpg")
                .withNormalTexturePath("Textures/2k_earth_normal_map.jpg")
                .withPosition(earthStart)
                .withApparentRadius(4)
                .withAssetManager(assetManager)
                .withMass(5.972E24)
                .withRotationSpeed(0.5f)
                .withRadius(10)
                .withVelocity(new Vector3f(29700, 0, 0)));
        addObject("Earth", earth);
        return earth;
    }

    private SpaceObject createMars() {
        Vector3f marsStart = new Vector3f(0, 0, 227_900_000_000f);

        Planet mars = new Planet(new SpaceObject.SpaceObjectBuilder("Mars")
                .withTexturePath("Textures/2k_mars.jpg")
                .withNormalTexturePath("Textures/2k_mars.png")
                .withPosition(marsStart)
                .withApparentRadius(2)
                .withAssetManager(assetManager)
                .withMass(6.41693E23)
                .withRotationSpeed(0.3f)
                .withRadius(10)
                .withVelocity(new Vector3f(24100, 0, 0)));
        addObject("Mars", mars);
        return mars;
    }

    private SpaceObject createJupiter() {
        Vector3f jupiterStart = new Vector3f(0, 0, 778_500_000_000f);

        Planet jupiter = new Planet(new SpaceObject.SpaceObjectBuilder("Jupiter")
                .withTexturePath("Textures/jupiter_diffuse.jpg")
                .withNormalTexturePath("Textures/jupiter_normal.png")
                .withPosition(jupiterStart)
                .withApparentRadius(8)
                .withAssetManager(assetManager)
                .withMass(1.898E27)
                .withRotationSpeed(0.5f)
                .withRadius(9)
                .withVelocity(new Vector3f(13070, 0, 0)));
        addObject("Jupiter", jupiter);
        return jupiter;
    }


    private void addObject(String name, SpaceObject spaceObject) {
        objects.put(name, spaceObject);
        universeNode.attachChild(spaceObject.getGlobe());
    }
}
