package feonr.logic.model.physicobject;

import com.jme3.asset.AssetManager;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import feonr.Ellipse;
import feonr.logic.model.physicobject.model.Orbit;

public class Path {
    String name;

    private Orbit orbit;
    private Orbit predictedPath;
    private Geometry orbitModel;
    private Geometry predictedPathModel;
    private Node pathNode;
    private AssetManager assetManager;
    private Node rootNode;
    private static int id = 0;

    public Path(Orbit orbit, AssetManager assetManager, Node rootNode) {
        this.name = "Orbit: " + id;
        this.orbit = orbit;
        this.orbitModel = Ellipse.of(Vector3f.ZERO, orbit, assetManager, 0);
        pathNode = new Node("Orbit: " + id);
        pathNode.attachChild(orbitModel);
        this.assetManager = assetManager;
        this.rootNode = rootNode;
        id++;
    }

    public void addExistingPaths(Node universeNode) {
        universeNode.attachChild(pathNode);
    }

    public String getName() {
        return name;
    }

    public Orbit getOrbit() {
        return orbit;
    }

    public void setPredictedPath(Orbit predictedPath, float angle) {
        if (predictedPathModel != null) {
            pathNode.detachChild(predictedPathModel);
            //pathNode.detachAllChildren();
        }
        this.predictedPath = predictedPath;
        this.predictedPathModel = Ellipse.of(Vector3f.ZERO, predictedPath, assetManager, angle, ColorRGBA.Red, this.name + "predict");
        //rootNode.attachChild(Ellipse.of(Vector3f.ZERO, predictedPath, assetManager, angle, ColorRGBA.Red, this.name + "predict"));

        pathNode.attachChild(this.predictedPathModel);
    }

    public Orbit getPredictedPath() {
        return predictedPath;
    }

    public Geometry getOrbitModel() {
        return orbitModel;
    }

    public Geometry getPredictedPathModel() {
        return predictedPathModel;
    }
}
