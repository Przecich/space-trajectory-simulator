package feonr.logic.model.physicobject;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import feonr.logic.model.physicobject.model.Orbit;

public class Planet extends SpaceObject{
    private Orbit path;
    public Planet(SpaceObjectBuilder builder){
        super(builder);
        path = new Orbit.OrbialBuilder()
                .withEccentricity(0)
                .withInclination(0)
                .withMajorAxis(builder.position.length()/1e9)
                .withPeriapsis(0)
                .withLongitude(0)
                .build();
    }

    public Orbit getPath(){
        return path;
    }

    protected  Material createMaterial(String textruePath, String normalTexturePath){
        if(assetManager==null)return null;
        Material sphereMat = new Material(assetManager,
                "Common/MatDefs/Light/Lighting.j3md");
        sphereMat.setTexture("DiffuseMap",
                assetManager.loadTexture(textruePath));
        sphereMat.setTexture("NormalMap",
                assetManager.loadTexture(normalTexturePath));
        sphereMat.setBoolean("UseMaterialColors",true);
        sphereMat.setColor("Diffuse", ColorRGBA.White);
        // sphereMat.setColor("Specular",ColorRGBA.White);
        sphereMat.setFloat("Shininess", 0.5f);  // [0,128]
        return sphereMat;
    }
}
