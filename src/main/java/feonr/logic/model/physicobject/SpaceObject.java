



package feonr.logic.model.physicobject;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;

import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Sphere;
import com.jme3.util.TangentBinormalGenerator;
import feonr.Circle;

abstract public class SpaceObject {

    protected AssetManager assetManager;
    private static final int scale = 1000000000;

    SpaceObject(SpaceObjectBuilder builder) {
        this.assetManager = builder.assetManager;
        this.mass = builder.mass;
        this.radius = builder.radius;
        this.position = builder.position;
        this.velocity = builder.velocity;
        this.rotationSpeed = builder.rotationSpeed;
        this.globe = createGlobe(builder.name, builder.texturePath, builder.normalTexturePath, scale(position), builder.apparentRadius);
        this.orbit = createOrbit(globe.getLocalTranslation().distance(Vector3f.ZERO));
    }

    private Spatial globe;
    private Spatial orbit;

    public void move(float tpf) {
        Vector3f divide = force.divide((float) mass);
        Vector3f mult = divide.mult(tpf);
        velocity = velocity.add(mult);

        force = Vector3f.ZERO;
        position = position.add(velocity.mult(tpf));
        globe.move(velocity.divide(scale).mult(tpf));
    }

    public void move2(float tpf) {
        Vector3f divide = force.divide((float) mass);
        Vector3f mult = divide.mult(tpf);
        velocity = velocity.add(mult);

        force = Vector3f.ZERO;
        position = position.add(velocity.mult(tpf));
    }

    public void rotate(float tpf){
        globe.rotate(0,0,tpf*rotationSpeed*FastMath.PI*2);
    }

    private final double mass;
    private final double radius;
    private final float rotationSpeed;
    private Vector3f position;
    private Vector3f velocity;
    private Vector3f force = Vector3f.ZERO;

    public void addForce(Vector3f fgrce){
        force = force.add(fgrce);
    }

    public double getMass() { return mass; }

    public double getRadius() {
        return radius;
    }

    public Vector3f getPosition() {
        return position;
    }

    public Spatial getGlobe() {
        return globe;
    }

    public Spatial getOrbit(){
        return orbit;
    }

    public void normalizeGlobe(){
        globe.scale(0.006f);
    }

    public void enlargeGlobe() {
        globe.scale(1/0.006f);
    }

    protected abstract Material createMaterial(String textruePath, String normalTexturePath);

    private Spatial createGlobe(String name, String textruePath, String normalTexturePath, Vector3f position, float apparentRadius) {
        Sphere sphereMesh = new Sphere(32, 32, apparentRadius);
        Geometry sphereGeo = new Geometry(name, sphereMesh);
        sphereMesh.setTextureMode(Sphere.TextureMode.Projected); // better quality on spheres
        TangentBinormalGenerator.generate(sphereMesh);           // for lighting effect

        sphereGeo.setMaterial(createMaterial(textruePath, normalTexturePath));
        sphereGeo.setLocalTranslation(position); // Move it a bit
        sphereGeo.rotate(-FastMath.PI / 2, 0, 0);          // Rotate it a bit

        return sphereGeo;
    }

    private Spatial createOrbit(float distance) {
        if(assetManager==null)return null;
        feonr.Circle circle = new Circle(new Vector3f(0f, 0f, 0f), distance, 100,0);
        Geometry geometry = new Geometry("Bullet", circle);
        Material orange = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");

        orange.setColor("Color", ColorRGBA.Orange);
        geometry.setMaterial(orange);
        return geometry;
    }

    public static class SpaceObjectBuilder {
        private String name;
        private String texturePath;
        private String normalTexturePath;
        protected Vector3f position;
        private float apparentRadius;
        private AssetManager assetManager;
        private double mass;
        private double radius;
        private Vector3f velocity;
        private float rotationSpeed;

        public SpaceObjectBuilder(String name) {
            this.name = name;
        }

        public SpaceObjectBuilder withTexturePath(String path) {
            this.texturePath = path;
            return this;
        }

        public SpaceObjectBuilder withNormalTexturePath(String path) {
            this.normalTexturePath = path;
            return this;
        }

        public SpaceObjectBuilder withPosition(Vector3f position) {
            this.position = position;
            return this;
        }

        public SpaceObjectBuilder withVelocity(Vector3f velocity) {
            this.velocity = velocity;
            return this;
        }

        public SpaceObjectBuilder withRadius(double radius) {
            this.radius = radius;
            return this;
        }

        public SpaceObjectBuilder withMass(double mass) {
            this.mass = mass;
            return this;
        }

        public SpaceObjectBuilder withApparentRadius(float apparentRadius) {
            this.apparentRadius = apparentRadius;
            return this;
        }

        public SpaceObjectBuilder withAssetManager(AssetManager assetManager) {
            this.assetManager = assetManager;
            return this;
        }

        public SpaceObjectBuilder withRotationSpeed(float rotationSpeed) {
            this.rotationSpeed = rotationSpeed;
            return this;
        }
    }

    private static Vector3f scale(Vector3f vector){
        return vector.divide(scale);
    }
}
