package feonr.logic.model.physicobject;
import com.jme3.material.Material;


public class Star extends SpaceObject{
    public Star(SpaceObjectBuilder builder){
        super(builder);
    }

    @Override
    protected Material createMaterial(String textruePath, String normalTexturePath){
        Material sphereMat = new Material(assetManager,
                "Common/MatDefs/Misc/Unshaded.j3md");
        sphereMat.setTexture("ColorMap",
                assetManager.loadTexture(textruePath));
        return sphereMat;
    }
}
