package feonr.logic.model.physicobject.model;

import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import feonr.Solver;
import feonr.logic.model.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.IntStream;

public class Orbit {
    private double semiMajorAxis;
    private double eccentricity;
    private double inclination;
    private double longitude;
    private double periapsis;

    public double getSemiMajorAxis() {
        return semiMajorAxis;
    }

    public double getEccentricity() {
        return eccentricity;
    }

    public double getInclination() {
        return inclination;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getPeriapsis() {
        return periapsis;
    }

    public double getMinorAxis() {
        return semiMajorAxis * Math.sqrt(1 - eccentricity * eccentricity);
    }

    public double calculateMeanAnomaly(double graviatationalParameter, double time) {
        double n = Math.sqrt(graviatationalParameter / (Math.pow(getSemiMajorAxis() * 1e9, 3)));
        return n * time;
    }

    public double calculateTrueAnomally(double graviatationalParameter, double time) {
        double M = calculateMeanAnomaly(graviatationalParameter, time);
        double e = getEccentricity();
        return eccentricToTrueAnomally(Solver.keplerEquation(e,M),e);
        //return M + (2 * e - 0.25 * Math.pow(e, 3)) * Math.sin(M) + 1.25 * Math.pow(e, 2) * Math.sin(2 * M) + (1.08333333333) * Math.pow(e, 3) * Math.sin(3 * M);
    }

    public Vector3f calculatePosition( float trueAnomaly){
        float e = (float) getEccentricity();
        float E = FastMath.acos((e + FastMath.cos(trueAnomaly)) / (1 + e * FastMath.cos(trueAnomaly)));

        double x = getSemiMajorAxis() * (FastMath.cos(E) - e);
        double z = getSemiMajorAxis() * Math.sqrt(1 - e * e) * FastMath.sin(E);

        float w = (float) getPeriapsis();
        float o = (float) getLongitude();
        float inc = (float) getInclination();
        double xx = ((FastMath.cos(w) * FastMath.cos(o) - FastMath.sin(w) * FastMath.sin(o) * FastMath.cos(inc)) * x)
                + ((-FastMath.sin(w) * FastMath.cos(o) - FastMath.cos(w) * FastMath.sin(o) * FastMath.cos(inc)) * z);

        double zz = ((FastMath.cos(w) * FastMath.sin(o) + FastMath.sin(w) * FastMath.cos(o) * FastMath.cos(inc)) * x)
                + ((-FastMath.sin(w) * FastMath.sin(o) + FastMath.cos(w) * FastMath.cos(o) * FastMath.cos(inc)) * z);

        double yy = FastMath.sin(w)*FastMath.sin(inc)*x+FastMath.cos(w)*FastMath.sin(inc)*z;
        return new Vector3f((float)xx,(float)zz,(float)yy);
    }

    public Vector3f calculateVelocity(float trueAnomaly){
        float e = (float) getEccentricity();
        float E = FastMath.acos((e + FastMath.cos(trueAnomaly)) / (1 + e * FastMath.cos(trueAnomaly)));

        double semiMajorAxis = this.semiMajorAxis*1e9;

        double ua = Math.sqrt(Constants.GM*semiMajorAxis);
        double rc = semiMajorAxis*(1-eccentricity*Math.cos(E));
        double urc = ua/rc;

        double x = urc*(-Math.sin(E));
        double z = urc*(Math.sqrt(1-e*e)*Math.cos(E));

        float w = (float) getPeriapsis();
        float o = (float) getLongitude();
        float inc = (float) getInclination();
        double xx = ((FastMath.cos(w) * FastMath.cos(o) - FastMath.sin(w) * FastMath.sin(o) * FastMath.cos(inc)) * x)
                + ((-FastMath.sin(w) * FastMath.cos(o) - FastMath.cos(w) * FastMath.sin(o) * FastMath.cos(inc)) * z);

        double zz = ((FastMath.cos(w) * FastMath.sin(o) + FastMath.sin(w) * FastMath.cos(o) * FastMath.cos(inc)) * x)
                + ((-FastMath.sin(w) * FastMath.sin(o) + FastMath.cos(w) * FastMath.cos(o) * FastMath.cos(inc)) * z);

        double yy = FastMath.sin(w)*FastMath.sin(inc)*x+FastMath.cos(w)*FastMath.sin(inc)*z;
        return new Vector3f((float)xx,(float)zz,(float)yy);
    }

    public double calculateRadius(double v) {
        return semiMajorAxis * ((1 - Math.pow(getEccentricity(), 2)) / (1 + getEccentricity() * Math.cos(v)));
    }

    private double calculateDistance(double graviataionalParameter, double time) {
        double radius = calculateRadius(calculateTrueAnomally(graviataionalParameter, time));
        return 60 - radius;
    }

    public double caluclateDistance(double gm, Orbit path,float time){
        float time1 =(float)calculateOrbitalPeriod(getSemiMajorAxis(), gm)/2;
        float trueAnomally1 = (float)calculateTrueAnomally(gm,time);
        float trueAnomally2 = (float)path.calculateTrueAnomally(gm,time);

       /* System.out.println("MENVOUR" +calculatePosition(trueAnomally1)+" HALF PERIOD "+calculateOrbitalPeriod(getSemiMajorAxis(), gm) / 2+" AN "+ trueAnomally1);

        System.out.println("EARTRH "+path.calculatePosition(trueAnomally2)+"HALF PERIOD" + +calculateOrbitalPeriod(path.getSemiMajorAxis(), gm) / 2+ " AN "+trueAnomally2);*/
        return calculatePosition(trueAnomally1).distance(path.calculatePosition(trueAnomally2));
    }

    public double reverseRadius(double radius){
        double semi = (semiMajorAxis*(1-eccentricity*eccentricity));
        return Math.acos(((semi/radius)-1)/eccentricity);
    }

    public double calculateCenterAngle(double trueAnomally){
        double b = calculateLinearEccentricity();
        if(b==0) return trueAnomally;
        double c = calculateRadius(trueAnomally);
        double asd = reverseRadius(c);
        double angle = FastMath.PI-trueAnomally;
        double a = Math.sqrt(Math.pow(b,2)+Math.pow(c,2)-2*b*c*Math.cos(angle));
        System.out.println(b);
        System.out.println(c);
        System.out.println(a);
        System.out.println("angle "+ angle);
        System.out.println( FastMath.asin((float) (Math.sin(trueAnomally)*c/a)));
        System.out.println(FastMath.acos((float)((b*b+a*a-c*c)/(2*b*a))));
        return FastMath.acos((float)((b*b+a*a-c*c)/(2*b*a)))/* FastMath.asin((float) (Math.sin(angle)*c/a))*/;
    }

    public double calculateLinearEccentricity(){
        return Math.sqrt(Math.pow(semiMajorAxis,2) - Math.pow(getMinorAxis(),2));
    }

    public double calculateOrbitalPeriod(double majorAxis, double graviatationalParameter) {
        return 2 * FastMath.PI * Math.sqrt(Math.pow(majorAxis * 1e9, 3) / graviatationalParameter);
    }

    public double calculateEscapeTime(double graviatationalParameter) {
        double halfOrbitPeriod = calculateOrbitalPeriod(getSemiMajorAxis(), graviatationalParameter) / 2;
        double a = 0;
        double b = halfOrbitPeriod;
        double c = 0;
        double e = calculateDistance(graviatationalParameter, 0);
        double iteration = 0;
        while (Math.abs(e) > 0.01 && iteration < 100) {
            c = (a + b) / 2;
            e = calculateDistance(graviatationalParameter,  c);
            if (e > 0) {
                a = c;
            } else {
                b = c;
            }
            iteration++;
        }
        System.out.println(e);
        return iteration >= 100 ? 0 : c;
    }

    public double calculateMinDistance(double graviatationalParameter, Orbit path) {
        double halfOrbitPeriod = calculateOrbitalPeriod(getSemiMajorAxis(), graviatationalParameter) / 2;
        double a = 0;
        double b = halfOrbitPeriod;
        double c = 0;
        Function<Double,Double> minDistance = x-> 0.615 -  caluclateDistance(graviatationalParameter, path,x.floatValue());
        double e =minDistance.apply(0.0);

        double iteration = 0;

        while (Math.abs(e) > 0.01 && iteration < 100) {
            c = (a + b) / 2;
            e = minDistance.apply(c);
            if (e > 0) {
                b = c;
            } else {
                a = c;
            }
            iteration++;
        }
        System.out.println("E"+e);
        System.out.println("C"+c);
        return iteration >= 500 ? 0 : c;
    }

    public Optional<Double> calculateMinDistance2(double graviatationalParameter, Orbit path) {
        Function<Double,Double> minDistance = x-> 0.615 -  caluclateDistance(graviatationalParameter, path,x.floatValue());
        //toDELETE VERY HEAVY
        List<Double> list = new ArrayList();
        for(int i =0;i<9941553;i+=3600){
            list.add(Math.abs(minDistance.apply((double)i)));
        }

        System.out.println("MIN DISTANCE"+list.stream().mapToDouble(x->x).min().getAsDouble());

        //

        double halfOrbitPeriod = calculateOrbitalPeriod(getSemiMajorAxis(), graviatationalParameter) / 2;
        System.out.println("HALF"+halfOrbitPeriod);
        return Solver.newtonRootSolver(minDistance, halfOrbitPeriod/2);

    }

    private double eccentricToTrueAnomally(double E, double e){
        return Math.acos(    (Math.cos(E) - e) / ( 1 - e*Math.cos(E))     );
    }

    private Orbit(OrbialBuilder builder) {
        this.semiMajorAxis = builder.semiMajorAxis;
        this.eccentricity = builder.eccentricity;
        this.inclination = builder.inclination;
        this.longitude = builder.longitude;
        this.periapsis = builder.periapsis;
    }

    public static class OrbialBuilder {
        private double semiMajorAxis;
        private double eccentricity;
        private double inclination;
        private double longitude;
        private double periapsis;

        public OrbialBuilder withMajorAxis(double axis) {
            this.semiMajorAxis = axis;
            return this;
        }

        public OrbialBuilder withEccentricity(double eccentricity) {
            this.eccentricity = eccentricity;
            return this;
        }

        public OrbialBuilder withInclination(double inclination) {
            this.inclination = inclination;
            return this;
        }

        public OrbialBuilder withLongitude(double longitude) {
            this.longitude = longitude;
            return this;
        }

        public OrbialBuilder withPeriapsis(double periapsis) {
            this.periapsis = periapsis;
            return this;
        }

        public Orbit build() {
            return new Orbit(this);
        }
    }
}
