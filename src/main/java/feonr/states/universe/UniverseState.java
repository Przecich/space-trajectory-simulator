package feonr.states.universe;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.collision.CollisionResults;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.input.*;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.light.Light;
import com.jme3.light.PointLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.math.Vector4f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.texture.Texture;
import com.jme3.util.SkyFactory;
import feonr.logic.model.Universe;
import feonr.logic.model.physicobject.SpaceObject;
import feonr.ui.SimulationDataUI;


public class UniverseState extends AbstractAppState {
    private final FlyByCamera flyCam;
    private Spatial skySpatial;
    private Node rootNode;
    private Node localRootNode = new Node("Universe");
    private Node guiNode;
    private InputManager inputManager;
    private AssetManager assetManager;
    private SimulationDataUI simUi;
    public Universe universe;
    private ChaseCamera chaseCam;
    private Vector4f cameraValues;
    private boolean scaled = true;


    public UniverseState(SimpleApplication app) {
        rootNode = app.getRootNode();
        assetManager = app.getAssetManager();
        guiNode = app.getGuiNode();
        flyCam = app.getFlyByCamera();
        inputManager = app.getInputManager();
        simUi = new SimulationDataUI(assetManager);
        universe = new Universe(localRootNode, assetManager);
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);

        addSky();
        guiNode.attachChild(simUi);
        simUi.updateSpeed(10);

        localRootNode.addLight(createLight());

        rootNode.attachChild(localRootNode);
        inputManager.addMapping("SPEED_UP", new KeyTrigger(KeyInput.KEY_F8));
        inputManager.addMapping("SELECT_OBJECT", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));
        inputManager.addMapping("TOGGLE_SCALE", new KeyTrigger(KeyInput.KEY_N));
        inputManager.addMapping("PAUSE", new KeyTrigger(KeyInput.KEY_P));

        final ActionListener actionListener = (name, keyPressed, tpf) -> {
            if (!keyPressed) {
                flyCam.setMoveSpeed(flyCam.getMoveSpeed() + 10.0f);
                simUi.updateSpeed((flyCam.getMoveSpeed()));
            }
        };

        app.getCamera().setFrustumFar(20000000);

        final AnalogListener analogListener = (name, intensity, tpf) -> {
           /* if (name.equals("SELECT_OBJECT")) {
                // Reset results list.
                CollisionResults results = new CollisionResults();
                // Aim the ray from camera location in camera direction
                // (assuming crosshairs in center of screen).
                Camera cam = app.getCamera();
                Ray ray = new Ray(cam.getLocation(), cam.getDirection());
                // Collect intersections between ray and all nodes in results list.
                rootNode.collideWith(ray, results);
                // Print the results so we see what is going on
                for (int i = 0; i < results.size(); i++) {
                    // For each “hit”, we know distance, impact point, geometry.
                    float dist = results.getCollision(i).getDistance();
                    Vector3f pt = results.getCollision(i).getContactPoint();
                    String target = results.getCollision(i).getGeometry().getName();
                    System.out.println("Selection #" + i + ": " + target + " at " + pt + ", " + dist + " WU away.");
                }
                if (results.size() > 0 && flyCam.isEnabled() && !results.getCollision(0).getGeometry().getName().equals("Sky")) {
                    Geometry geometry = results.getCollision(0).getGeometry();
                    //universe.getObjects().get(geometry.getName()).normalizeGlobe();
                    universe.getObjects().values().forEach(x->x.normalizeGlobe());
                    flyCam.setEnabled(false);
                    rootNode.detachChild(skySpatial);

                    // Enable a chase cam

                    chaseCam = new ChaseCamera(cam, geometry, inputManager);
                    chaseCam.setDefaultDistance(20f);
                    chaseCam.setMaxDistance(500);
                    //chaseCam.setZoomSensitivity(0.06f);
                    chaseCam.setInvertVerticalAxis(true);
                    float a = 0.002f;


                    saveCamera(cam);
                    cam.setFrustumBottom(cam.getFrustumBottom() * a);
                    cam.setFrustumLeft(cam.getFrustumLeft() * a);
                    cam.setFrustumTop(cam.getFrustumTop() * a);
                    cam.setFrustumRight(cam.getFrustumRight() * a);



                }
            }*/
        };

        final ActionListener togleAction = (name, keyPressed, tpf) -> {
            if(!keyPressed) {
                if (scaled) {
                    universe.getObjects().values().forEach(SpaceObject::normalizeGlobe);
                    scaled = false;
                } else {
                    universe.getObjects().values().forEach(SpaceObject::enlargeGlobe);
                    scaled = true;
                }
            }
        };

        final ActionListener toglePause = (name, keyPressed, tpf) -> {
            if(!keyPressed) {
                if (universe.isSimulationRun()) universe.setSimulationRun(false);
                else universe.setSimulationRun(true);
            }
        };

        initCrossHairs();
        inputManager.addListener(actionListener,"SPEED_UP");
        inputManager.addListener(analogListener, "SELECT_OBJECT");
        inputManager.addListener(togleAction, "TOGGLE_SCALE");
        inputManager.addListener(toglePause, "PAUSE");
    }

    public Node getUniverseNode(){
        return localRootNode;
    }

    @Override
    public void update(float tpf) {
        universe.update(tpf);
    }

    @Override
    public void cleanup() {
        rootNode.detachChild(localRootNode);

        super.cleanup();
    }

    protected void initCrossHairs() {
        BitmapFont guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
        BitmapText ch = new BitmapText(guiFont, false);
        ch.setSize(guiFont.getCharSet().getRenderedSize() * 2);
        ch.setText("+");
        ch.setLocalTranslation( // center
                1280 / 2 - ch.getLineWidth() / 2,
                720 / 2 + ch.getLineHeight() / 2, 0);
        guiNode.attachChild(ch);
    }

    private Light createLight() {
        PointLight lamp_light = new PointLight();
        lamp_light.setColor(ColorRGBA.White);
        lamp_light.setPosition(new Vector3f(0, 0, 0));
        return lamp_light;
    }

    private void addSky() {
        Texture westTex = assetManager.loadTexture("Textures/Sky/Milkyway/med/west.jpg");
        Texture eastTex = assetManager.loadTexture("Textures/Sky/Milkyway/med/east.jpg");
        Texture northTex = assetManager.loadTexture("Textures/Sky/Milkyway/med/north.jpg");
        Texture southTex = assetManager.loadTexture("Textures/Sky/Milkyway/med/south.jpg");
        Texture upTex = assetManager.loadTexture("Textures/Sky/Milkyway/med/up.jpg");
        Texture downTex = assetManager.loadTexture("Textures/Sky/Milkyway/med/down.jpg");

        final Vector3f normalScale = new Vector3f(-1, 1, 1);
        skySpatial = SkyFactory.createSky(
                assetManager,
                westTex,
                eastTex,
                northTex,
                southTex,
                upTex,
                downTex,
                normalScale);

        rootNode.attachChild(skySpatial);
    }

    private void saveCamera(Camera cam) {
        cameraValues = new Vector4f(cam.getFrustumBottom(), cam.getFrustumLeft(), cam.getFrustumTop(), cam.getFrustumRight());
    }
}
