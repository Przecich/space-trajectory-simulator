package feonr.ui;

import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapFont;
import com.jme3.font.BitmapText;
import com.jme3.scene.Node;

public class SimulationDataUI extends Node {
    private BitmapText speedLine;
    private AssetManager assetManager;

    public SimulationDataUI(AssetManager assetManager){
        this.assetManager = assetManager;
        this.name = "SimUI";
        setLocalTranslation(300,0,0);
        speedLine = createSpeedLine();
        attachChild(createHeader());
        attachChild(speedLine);
    }

    public void updateSpeed(float speed){
        speedLine.setText("Speed: "+speed);
    }

    private BitmapText createSpeedLine(){
        BitmapText secondLine = createText("Speed : ");
        secondLine.setLocalTranslation(0,secondLine.getLineHeight(),0);
        secondLine.scale(0.8f);
        return secondLine;
    }

    private BitmapText createHeader(){

        BitmapText firstLine = createText("Sim Data : ");
        firstLine.setLocalTranslation(0,firstLine.getLineHeight()*2,0);
        return firstLine;
    }

    private BitmapText createText(String text){
        BitmapFont guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");

        BitmapText helloText = new BitmapText(guiFont, false);

        helloText.setSize(guiFont.getCharSet().getRenderedSize());
        helloText.setText(text);
        return helloText;
    }
}
